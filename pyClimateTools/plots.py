# -*- coding: utf-8 -*-

import numpy as np
from pyClimateTools.funcoes import rolling_window, gridindex
from pyClimateTools.estatistica import composites_t_diff, composites_t_test
# from PyFuncemeClimateTools import DefineGrid as Dg
import matplotlib.colors as clr
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap


def composite_plot(data, lat, lon, significance_level, **kwargs):
    """
    Calcula composição do dado de entrada, plota o dado (contorno) e a
    significância estatistica do dado (fill).
    => Parâmetros obrigatórios:
    data          = matriz a ser feita composição (np.array 3d)
    lat           = pontos da grade na latitude (np.array) (-90:90)
    lon           = pontos da grade na longitude (np.array) (-180:180)
    significance_level = nível de significância a ser utilizado no teste t (int)
    """

    # Parametros opcionais
    my_title = kwargs.pop('fig_title', '')
    fig_name = kwargs.pop('fig_name', 'my_picture.png')
    var_name = kwargs.pop('var_name', '')
    barlevels = kwargs.pop('barlevels', None)
    noparallels = kwargs.pop('noparallels', False)
    nomeridians = kwargs.pop('nomeridians', False)
    parallels = kwargs.pop('parallels', np.arange(-90., 100., 10.))
    meridians = kwargs.pop('meridians', np.arange(-180., 200., 20.))
    frmt = kwargs.pop('frmt', '%1.1f')
    bold_zero = kwargs.pop(False, True)

    composite = np.nanmean(data, 0)
    mask = composites_t_test(data, significance_level)

    fig = plt.figure(figsize=(12, 10))
    ax = fig.add_subplot(111)
    cmap = ['#FFFFFF', '#989898']
    my_cmap = clr.ListedColormap(cmap)

    x, y = np.meshgrid(lon, lat)
    m = Basemap(np.min(lon), np.min(lat), np.max(lon), np.max(lat))
    m.drawcoastlines()
    if noparallels:
        m.drawparallels(parallels, labels=[0, 0, 0, 0], linewidth=0.001, fontsize=8)
    else:
        m.drawparallels(parallels, labels=[1, 0, 1, 0], linewidth=0.001, fontsize=8)
    if nomeridians:
        m.drawmeridians(meridians, labels=[0, 0, 0, 0], linewidth=0.001, fontsize=8)
    else:
        m.drawmeridians(meridians, labels=[0, 1, 0, 1], linewidth=0.001, fontsize=8)
    plt.title(my_title)
    ax.text(1, 1, var_name, transform=ax.transAxes,
            horizontalalignment='right', verticalalignment='bottom')
    c = plt.contour(x, y, composite, colors='k', levels=barlevels)
    plt.clabel(c, fontsize=7, inline=1, inline_spacing=5, fmt=frmt)
    if bold_zero:
        zero = plt.contour(x, y, composite, levels=(0,), colors='black',
                           linewidths=2)
        plt.clabel(zero, fontsize=7, inline=1, inline_spacing=5, fmt=frmt)
    plt.contourf(x, y, mask, cmap=my_cmap)
    plt.savefig(fig_name, bbox_inches='tight')


def composite_plot_colored(data, lat, lon, significance_level, subplot=False, **kwargs):
    """
    Calcula composição do dado de entrada, plota o dado (contorno) e a
    significância estatistica do dado (fill).
    => Parâmetros obrigatórios:
    data          = matriz a ser feita composição (np.array 3d)
    lat           = pontos da grade na latitude (np.array) (-90:90)
    lon           = pontos da grade na longitude (np.array) (-180:180)
    significance_level = nível de significância a ser utilizado no teste t (int)
    """

    # Parametros opcionais
    my_title = kwargs.pop('fig_title', '')
    fig_name = kwargs.pop('fig_name', 'my_picture.png')
    var_name = kwargs.pop('var_name', '')
    noparallels = kwargs.pop('noparallels', False)
    nomeridians = kwargs.pop('nomeridians', False)
    parallels = kwargs.pop('parallels', np.arange(-90., 100., 10.))
    meridians = kwargs.pop('meridians', np.arange(-180., 200., 20.))
    cmap = kwargs.pop('cmap', ('#000044', '#0033FF', '#007FFF', '#0099FF', '#00B2FF', '#00CCFF', '#FFFFFF',
                               '#FFFFFF', '#FFCC00', '#FF9900', '#FF7F00', '#FF3300', '#A50000', '#B48C82'))
    levels = kwargs.pop('levels', np.linspace(np.min(data), np.max(data), 13))
    draw_states = kwargs.pop('states', False)
    # mask_data = kwargs.pop('mask_data', False)

    composite = np.nanmean(data, 0)
    sig = composites_t_test(data, significance_level)
    # if mask_data:
    #     Ptsgrid, lonlatgrid, Ptmask = Dg.pointinside(lat, lon, shapefile=mask_data, delimiter=' ')
    #     composite = np.ma.array(composite, mask=Ptmask)
    #     #sig = np.ma.array(sig, mask=Ptmask)

    if not subplot:
        fig = plt.figure(figsize=(12, 10))
        ax = fig.add_subplot(111)
    
    my_cmap = clr.ListedColormap(cmap)
    bar1 = cmap
    cmapover = cmap[-1]
    cmapunder = cmap[0]
    bar1 = bar1[1:-1]
    my_cmap = clr.ListedColormap(bar1)
    my_cmap.set_over(cmapover)
    my_cmap.set_under(cmapunder)
    norm = clr.BoundaryNorm(levels, my_cmap.N, clip=False)

    x, y = np.meshgrid(lon, lat)
    m = Basemap(np.min(lon), np.min(lat), np.max(lon), np.max(lat))
    if draw_states:
        m.drawstates()
    m.drawcoastlines()
    if noparallels:
        m.drawparallels(parallels, labels=[0, 0, 0, 0], linewidth=0.5, fontsize=8)
    else:
        m.drawparallels(parallels, labels=[1, 0, 1, 0], linewidth=0.5, fontsize=8)
    if nomeridians:
        m.drawmeridians(meridians, labels=[0, 0, 0, 0], linewidth=0.5, fontsize=8)
    else:
        m.drawmeridians(meridians, labels=[0, 1, 0, 1], linewidth=0.5, fontsize=8)
    plt.title(my_title)
    if not subplot:
        ax.text(1, 1, var_name, transform=ax.transAxes,
                horizontalalignment='right', verticalalignment='bottom')
    c = plt.contourf(x, y, composite, cmap=my_cmap, levels=levels, norm=norm, extend='both')
    plt.contour(x, y, sig, linewidths=2, colors='k', levels=[0, 1])
    if not subplot:
        plt.colorbar(c, ticks=levels, orientation='horizontal', pad=0.05)
        plt.savefig(fig_name, bbox_inches='tight')
    else:
        return c


def composite_difference_colored(data1, data2, lat, lon, significance_level, subplot=False, **kwargs):
    """
    Calcula composição do dado de entrada, plota o dado (contorno) e a
    significância estatistica do dado (fill).
    => Parâmetros obrigatórios:
    data1          = matriz a ser feita a diferenca (np.array 3d)
    data2          = matriz a ser feita a diferenca (np.array 3d)
    lat           = pontos da grade na latitude (np.array) (-90:90)
    lon           = pontos da grade na longitude (np.array) (-180:180)
    significance_level = nível de significância a ser utilizado no teste t (int)
    """

    # Parametros opcionais
    my_title = kwargs.pop('fig_title', '')
    fig_name = kwargs.pop('fig_name', 'my_picture.png')
    var_name = kwargs.pop('var_name', '')
    noparallels = kwargs.pop('noparallels', False)
    nomeridians = kwargs.pop('nomeridians', False)
    parallels = kwargs.pop('parallels', np.arange(-90., 100., 10.))
    meridians = kwargs.pop('meridians', np.arange(-180., 200., 20.))
    cmap = kwargs.pop('cmap', ('#000044', '#0033FF', '#007FFF', '#0099FF', '#00B2FF', '#00CCFF', '#FFFFFF',
                               '#FFFFFF', '#FFCC00', '#FF9900', '#FF7F00', '#FF3300', '#A50000', '#B48C82'))
    levels = kwargs.pop('levels', np.linspace(np.min(np.nanmean(data1, 0) - np.nanmean(data2, 0)), np.max(np.nanmean(data1, 0) - np.nanmean(data2, 0)), 13))
    draw_states = kwargs.pop('states', False)
    # mask_data = kwargs.pop('mask_data', False)

    composite = np.nanmean(data1, 0) - np.nanmean(data2, 0)
    sig = composites_t_diff(data1, data2, significance_level)
    # if mask_data:
    #     Ptsgrid, lonlatgrid, Ptmask = Dg.pointinside(lat, lon, shapefile=mask_data, delimiter=' ')
    #     composite = np.ma.array(composite, mask=Ptmask)
    #     #sig = np.ma.array(sig, mask=Ptmask)

    if not subplot:
        fig = plt.figure(figsize=(12, 10))
        ax = fig.add_subplot(111)
    
    my_cmap = clr.ListedColormap(cmap)
    bar1 = cmap
    cmapover = cmap[-1]
    cmapunder = cmap[0]
    bar1 = bar1[1:-1]
    my_cmap = clr.ListedColormap(bar1)
    my_cmap.set_over(cmapover)
    my_cmap.set_under(cmapunder)
    norm = clr.BoundaryNorm(levels, my_cmap.N, clip=False)

    x, y = np.meshgrid(lon, lat)
    m = Basemap(np.min(lon), np.min(lat), np.max(lon), np.max(lat))
    if draw_states:
        m.drawstates()
    m.drawcoastlines()
    if noparallels:
        m.drawparallels(parallels, labels=[0, 0, 0, 0], linewidth=0.5, fontsize=8)
    else:
        m.drawparallels(parallels, labels=[1, 0, 1, 0], linewidth=0.5, fontsize=8)
    if nomeridians:
        m.drawmeridians(meridians, labels=[0, 0, 0, 0], linewidth=0.5, fontsize=8)
    else:
        m.drawmeridians(meridians, labels=[0, 1, 0, 1], linewidth=0.5, fontsize=8)
    plt.title(my_title)
    if not subplot:
        ax.text(1, 1, var_name, transform=ax.transAxes,
                horizontalalignment='right', verticalalignment='bottom')
    c = plt.contourf(x, y, composite, cmap=my_cmap, levels=levels, norm=norm, extend='both')
    plt.contour(x, y, sig, linewidths=2, colors='k', levels=[0, 1])
    if not subplot:
        plt.colorbar(c, ticks=levels, orientation='horizontal', pad=0.05)
        plt.savefig(fig_name, bbox_inches='tight')
    else:
        return c


def wind_composite_plot(udata, vdata, lat, lon, significance_level, subplot=False, **kwargs):
    """
    Calcula composição do dado de entrada, plota o dado (contorno) e a
    significância estatistica do dado (fill).
    => Parâmetros obrigatórios:
    udata          = matriz a ser feita composição (np.array 3d)
    vdata          = matriz a ser feita composição (np.array 3d)
    lat           = pontos da grade na latitude (np.array) (-90:90)
    lon           = pontos da grade na longitude (np.array) (-180:180)
    significance_level = nível de significância a ser utilizado no teste t (int)
    """

    # Parametros opcionais
    my_title = kwargs.pop('fig_title', '')
    noparallels = kwargs.pop('noparallels', False)
    nomeridians = kwargs.pop('nomeridians', False)
    parallels = kwargs.pop('parallels', np.arange(-90., 91., 10.))
    meridians = kwargs.pop('meridians', np.arange(-160., 161., 20.))
    fig_name = kwargs.pop('fig_name', 'my_picture.png')
    var_name = kwargs.pop('var_name', '')
    quiver_scale = kwargs.pop('scale', 25)

    ucomposite = np.nanmean(udata, 0)
    vcomposite = np.nanmean(vdata, 0)
    umask = composites_t_test(udata, significance_level)
    vmask = composites_t_test(vdata, significance_level)
    mask = np.where(umask + vmask == 2, 1, umask + vmask)

    if not subplot:
        fig = plt.figure(figsize=(12, 10))
        ax = fig.add_subplot(111)

    cmap = ['#FFFFFF', '#989898']
    my_cmap = clr.ListedColormap(cmap)

    x, y = np.meshgrid(lon, lat)
    m = Basemap(np.min(lon), np.min(lat), np.max(lon), np.max(lat))
    m.drawcoastlines()
    if noparallels:
        m.drawparallels(parallels, labels=[0, 0, 0, 0], linewidth=0.001, fontsize=8)
    else:
        m.drawparallels(parallels, labels=[1, 0, 1, 0], linewidth=0.001, fontsize=8)
    if nomeridians:
        m.drawmeridians(meridians, labels=[0, 0, 0, 0], linewidth=0.001, fontsize=8)
    else:
        m.drawmeridians(meridians, labels=[0, 1, 0, 1], linewidth=0.001, fontsize=8)
    plt.title(my_title)
    if not subplot:
        ax.text(1, 1, var_name, transform=ax.transAxes,
                horizontalalignment='right', verticalalignment='bottom')
    # plt.contourf(x, y, mask, cmap=my_cmap)
    Q = plt.quiver(lon[:], lat[:], ucomposite[:, :],
                   vcomposite[:, :], scale=quiver_scale, headwidth=7, headlength=7)
    plt.quiverkey(Q, 0.1, 1.03, 1, '1 m/s', labelpos='E', coordinates='axes')
    if not subplot:
        plt.savefig(fig_name, bbox_inches='tight')


def wind_composite_plot_colored(udata, vdata, lat, lon, significance_level, subplot=False, **kwargs):
    """
    Calcula composição do dado de entrada, plota o dado (contorno) e a
    significância estatistica do dado (fill).
    => Parâmetros obrigatórios:
    udata          = matriz a ser feita composição (np.array 3d)
    vdata          = matriz a ser feita composição (np.array 3d)
    lat           = pontos da grade na latitude (np.array) (-90:90)
    lon           = pontos da grade na longitude (np.array) (-180:180)
    significance_level = nível de significância a ser utilizado no teste t (int)
    """

    # Parametros opcionais
    my_title = kwargs.pop('fig_title', '')
    noparallels = kwargs.pop('noparallels', False)
    nomeridians = kwargs.pop('nomeridians', False)
    parallels = kwargs.pop('parallels', np.arange(-90., 91., 10.))
    meridians = kwargs.pop('meridians', np.arange(-160., 161., 20.))
    fig_name = kwargs.pop('fig_name', 'my_picture.png')
    var_name = kwargs.pop('var_name', '')
    quiver_scale = kwargs.pop('scale', 25)
    cmap = kwargs.pop('cmap', ('#FFFFFF', '#00CCFF', '#0099FF', '#0000FF', '#FFB200', '#FF0000', '#A50000'))
    levels = kwargs.pop('levels', np.linspace(0., 3.5, 6))
    normalize = kwargs.pop('norm_vectors', False)

    ucomposite = np.nanmean(udata, 0)
    vcomposite = np.nanmean(vdata, 0)
    umask = composites_t_test(udata, significance_level)
    vmask = composites_t_test(vdata, significance_level)
    mask = np.where(umask + vmask == 2, 1, umask + vmask)

    if not subplot:
        fig = plt.figure(figsize=(12, 10))
        ax = fig.add_subplot(111)
    my_cmap = clr.ListedColormap(cmap)
    bar1 = cmap
    cmapover = cmap[-1]
    bar1 = bar1[0:-1]
    my_cmap = clr.ListedColormap(bar1)
    my_cmap.set_over(cmapover)
    norm = clr.BoundaryNorm(levels, my_cmap.N, clip=False)

    x, y = np.meshgrid(lon, lat)
    m = Basemap(np.min(lon), np.min(lat), np.max(lon), np.max(lat))
    m.drawcoastlines()
    if noparallels:
        m.drawparallels(parallels, labels=[0, 0, 0, 0], linewidth=0.001, fontsize=8)
    else:
        m.drawparallels(parallels, labels=[1, 0, 1, 0], linewidth=0.001, fontsize=8)
    if nomeridians:
        m.drawmeridians(meridians, labels=[0, 0, 0, 0], linewidth=0.001, fontsize=8)
    else:
        m.drawmeridians(meridians, labels=[0, 1, 0, 1], linewidth=0.001, fontsize=8)
    plt.title(my_title)
    if not subplot:
        ax.text(1, 1, var_name, transform=ax.transAxes,
                horizontalalignment='right', verticalalignment='bottom')
    result = np.sqrt((ucomposite**2) + (vcomposite**2))
    if normalize:
        ucomposite = ucomposite/result
        vcomposite = vcomposite/result
    c = plt.contourf(x, y, result, cmap=my_cmap, levels=levels, norm=norm, extend='max')
    Q = plt.quiver(lon[:], lat[:], ucomposite[:, :], vcomposite[:, :], scale=quiver_scale, headwidth=7, headlength=7)
    # plt.contour(x, y, mask, linewidths=2, colors='k', levels=[0, 1])
    # plt.quiverkey(Q, 0.1, 1.03, 1, '1 m/s', labelpos='E', coordinates='axes')
    if not subplot:
        plt.colorbar(c, ticks=levels)
        plt.savefig(fig_name, bbox_inches='tight')

    return c


def composite_subplots(data, lat, lon, significance_level, **kwargs):
    """
    Calcula composição do dado de entrada, plota o dado (contorno) e a
    significância estatistica do dado (fill).
    => Parâmetros obrigatórios:
    data          = matriz a ser feita composição (np.array 3d)
    lat           = pontos da grade na latitude (np.array) (-90:90)
    lon           = pontos da grade na longitude (np.array) (-180:180)
    significance_level = nível de significância a ser utilizado no teste t (int)
    """

    # Parametros opcionais
    my_title = kwargs.pop('fig_title', '')
    var_name = kwargs.pop('var_name', '')
    barlevels = kwargs.pop('barlevels', None)
    parallels = kwargs.pop('parallels', True)
    meridians = kwargs.pop('meridians', True)
    frmt = kwargs.pop('frmt', '%1.1f')
    bold_zero = kwargs.pop(False, True)

    composite = np.nanmean(data, 0)
    mask = composites_t_test(data, significance_level)

    cmap = ['#FFFFFF', '#989898']
    my_cmap = clr.ListedColormap(cmap)

    x, y = np.meshgrid(lon, lat)
    m = Basemap(np.min(lon), np.min(lat), np.max(lon), np.max(lat))
    m.drawcoastlines()
    if parallels:
        m.drawparallels(np.arange(-90., 100., 10.), labels=[1, 0, 1, 0], linewidth=0.001, fontsize=8)
    if meridians:
        m.drawmeridians(np.arange(-180., 200., 20.), labels=[0, 1, 0, 1], linewidth=0.001, fontsize=8)
    plt.title(my_title)
    c = plt.contour(x, y, composite, colors='k', levels=barlevels)
    plt.clabel(c, fontsize=7, inline=1, inline_spacing=5, fmt=frmt)
    if bold_zero:
        zero = plt.contour(x, y, composite, levels=(0,), colors='black',
                           linewidths=2)
        plt.clabel(zero, fontsize=7, inline=1, inline_spacing=5, fmt=frmt)
    plt.contourf(x, y, mask, cmap=my_cmap)


def composite_subplots_colored(data, lat, lon, significance_level, **kwargs):
    """
    Calcula composição do dado de entrada, plota o dado (contorno) e a
    significância estatistica do dado (fill).
    => Parâmetros obrigatórios:
    data          = matriz a ser feita composição (np.array 3d)
    lat           = pontos da grade na latitude (np.array) (-90:90)
    lon           = pontos da grade na longitude (np.array) (-180:180)
    significance_level = nível de significância a ser utilizado no teste t (int)
    """

    # Parametros opcionais
    my_title = kwargs.pop('fig_title', '')
    var_name = kwargs.pop('var_name', '')
    levels = kwargs.pop('barlevels', None)
    parallels = kwargs.pop('parallels', True)
    meridians = kwargs.pop('meridians', True)
    cbar = kwargs.pop('cbar', True)
    frmt_cbar = kwargs.pop('fmt_cbar', '%1.1f')
    cmap = kwargs.pop('cmap', ('#000044', '#0033FF', '#007FFF', '#0099FF', '#00B2FF', '#00CCFF', '#FFFFFF',
                               '#FFFFFF', '#FFCC00', '#FF9900', '#FF7F00', '#FF3300', '#A50000', '#B48C82'))

    composite = np.nanmean(data, 0)
    mask = composites_t_test(data, significance_level)

    my_cmap = clr.ListedColormap(cmap)
    bar1 = cmap
    cmapover = cmap[-1]
    cmapunder = cmap[0]
    bar1 = bar1[1:-1]
    my_cmap = clr.ListedColormap(bar1)
    my_cmap.set_over(cmapover)
    my_cmap.set_under(cmapunder)
    norm = clr.BoundaryNorm(levels, my_cmap.N, clip=False)

    x, y = np.meshgrid(lon, lat)
    m = Basemap(np.min(lon), np.min(lat), np.max(lon), np.max(lat))
    m.drawcoastlines()
    if parallels:
        m.drawparallels(np.arange(-90., 100., 10.), labels=[1, 0, 1, 0], linewidth=0.001, fontsize=8)
    if meridians:
        m.drawmeridians(np.arange(-180., 200., 20.), labels=[0, 1, 0, 1], linewidth=0.001, fontsize=8)
    plt.title(my_title)
    c = plt.contourf(x, y, composite, cmap=my_cmap, levels=levels, norm=norm, extend='both')
    plt.contour(x, y, mask, linewidths=2, colors='k', levels=[0, 1])
    if cbar:
        cb = plt.colorbar(c, ticks=levels, orientation='horizontal', format=frmt_cbar)
        cb.ax.tick_params(labelsize=7)


def wind_composite_subplots(udata, vdata, lat, lon, significance_level, **kwargs):
    """
    Calcula composição do dado de entrada, plota o dado (contorno) e a
    significância estatistica do dado (fill).
    => Parâmetros obrigatórios:
    udata          = matriz a ser feita composição (np.array 3d)
    vdata          = matriz a ser feita composição (np.array 3d)
    lat           = pontos da grade na latitude (np.array) (-90:90)
    lon           = pontos da grade na longitude (np.array) (-180:180)
    significance_level = nível de significância a ser utilizado no teste t (int)
    """

    # Parametros opcionais
    my_title = kwargs.pop('fig_title', '')
    parallels = kwargs.pop('parallels', True)
    meridians = kwargs.pop('meridians', True)
    var_name = kwargs.pop('var_name', '')
    quiver_scale = kwargs.pop('scale', 25)

    ucomposite = np.nanmean(udata, 0)
    vcomposite = np.nanmean(vdata, 0)
    umask = composites_t_test(udata, significance_level)
    vmask = composites_t_test(vdata, significance_level)
    mask = np.where(umask + vmask == 2, 1, umask + vmask)

    cmap = ['#FFFFFF', '#989898']
    my_cmap = clr.ListedColormap(cmap)

    x, y = np.meshgrid(lon, lat)
    m = Basemap(np.min(lon), np.min(lat), np.max(lon), np.max(lat))
    m.drawcoastlines()
    if parallels:
        m.drawparallels(np.arange(-90., 91., 10.), labels=[1, 0, 1, 0], linewidth=0.001, fontsize=8)
    if meridians:
        m.drawmeridians(np.arange(-160., 161., 20.), labels=[0, 1, 0, 1], linewidth=0.001, fontsize=8)
    plt.title(my_title)
    plt.contourf(x, y, mask, cmap=my_cmap)
    Q = plt.quiver(lon[:], lat[:], ucomposite[:, :],
                   vcomposite[:, :], scale=quiver_scale, headwidth=7, headlength=7)
    plt.quiverkey(Q, 0.1, 1.03, 1, '1 m/s', labelpos='E', coordinates='axes')


def wind_composite_subplots_colored(udata, vdata, lat, lon, significance_level, **kwargs):
    """
    Calcula composição do dado de entrada, plota o dado (contorno) e a
    significância estatistica do dado (fill).
    => Parâmetros obrigatórios:
    udata          = matriz a ser feita composição (np.array 3d)
    vdata          = matriz a ser feita composição (np.array 3d)
    lat           = pontos da grade na latitude (np.array) (-90:90)
    lon           = pontos da grade na longitude (np.array) (-180:180)
    significance_level = nível de significância a ser utilizado no teste t (int)
    """

    # Parametros opcionais
    my_title = kwargs.pop('fig_title', '')
    parallels = kwargs.pop('parallels', True)
    meridians = kwargs.pop('meridians', True)
    var_name = kwargs.pop('var_name', '')
    quiver_scale = kwargs.pop('scale', 25)
    cmap = kwargs.pop('cmap', ('#FFFFFF', '#00CCFF', '#0099FF', '#0000FF', '#FFB200', '#FF0000', '#A50000'))
    levels = kwargs.pop('barlevels', np.linspace(0., 3.5, 6))
    cbar = kwargs.pop('cbar', True)
    frmt_cbar = kwargs.pop('fmt_cbar', '%1.1f')
    normalize = kwargs.pop('norm_vector', False)

    ucomposite = np.nanmean(udata, 0)
    vcomposite = np.nanmean(vdata, 0)
    umask = composites_t_test(udata, significance_level)
    vmask = composites_t_test(vdata, significance_level)
    mask = np.where(umask + vmask == 2, 1, umask + vmask)

    my_cmap = clr.ListedColormap(cmap)
    bar1 = cmap
    cmapover = cmap[-1]
    bar1 = bar1[0:-1]
    my_cmap.set_over(cmapover)
    norm = clr.BoundaryNorm(levels, my_cmap.N, clip=False)

    x, y = np.meshgrid(lon, lat)
    m = Basemap(np.min(lon), np.min(lat), np.max(lon), np.max(lat))
    m.drawcoastlines()
    if parallels:
        m.drawparallels(np.arange(-90., 91., 10.), labels=[1, 0, 1, 0], linewidth=0.001, fontsize=8)
    if meridians:
        m.drawmeridians(np.arange(-160., 161., 20.), labels=[0, 1, 0, 1], linewidth=0.001, fontsize=8)
    plt.title(my_title)
    result = np.sqrt((ucomposite**2) + (vcomposite**2))
    if normalize:
        ucomposite = ucomposite/result
        vcomposite = vcomposite/result
    c = plt.contourf(x, y, result, cmap=my_cmap, levels=levels, norm=norm, extend='max')
    Q = plt.quiver(lon[:], lat[:], ucomposite[:, :], vcomposite[:, :], scale=quiver_scale, headwidth=7, headlength=7)
    plt.contour(x, y, mask, linewidths=2, colors='k', levels=[0, 1])
    plt.quiverkey(Q, 0.1, 1.03, 1, '1 m/s', labelpos='E', coordinates='axes')
    if cbar:
        cb = plt.colorbar(c, ticks=levels, orientation='horizontal', format=frmt_cbar)
        cb.ax.tick_params(labelsize=7)


def wind_composite_vertical_plot(wdata, z, lon, significance_level, **kwargs):
    """
    Calcula composição do dado de entrada, plota o dado (contorno) e a
    significância estatistica do dado (fill).
    => Parâmetros obrigatórios:
    udata          = matriz a ser feita composição (np.array 3d)
    vdata          = matriz a ser feita composição (np.array 3d)
    lat           = pontos da grade na latitude (np.array) (-90:90)
    lon           = pontos da grade na longitude (np.array) (-180:180)
    significance_level = nível de significância a ser utilizado no teste t (int)
    """

    # Parametros opcionais
    my_title = kwargs.pop('fig_title', '')
    fig_name = kwargs.pop('fig_name', 'my_picture.png')
    var_name = kwargs.pop('var_name', '')
    frmt = kwargs.pop('frmt', '%1.1f')
    barlevels = kwargs.pop('barlevels', None)

    wcomposite = np.nanmean(wdata, 0)
    wmask = composites_t_test(wdata, significance_level)

    lons = []
    for i in lon:
        if -180 < i < 0:
            lons.append('{0}W'.format(int(np.abs(i))))
        elif i == 0.:
            lons.append('0')
        elif i == 180.:
            lons.append('180')
        else:
            if i < 0:
                lons.append('{0}E'.format(int(np.abs(i + 360))))
            else:
                lons.append('{0}E'.format(int(i)))

    fig = plt.figure(figsize=(12, 10))
    ax1 = fig.add_subplot(111)
    cmap = ['#FFFFFF', '#989898']
    my_cmap = clr.ListedColormap(cmap)
    x, y = np.meshgrid(lon, z)
    plt.title(my_title)
    ax1.text(1, 1, var_name, transform=ax1.transAxes, horizontalalignment='right', verticalalignment='bottom')
    plt.contourf(x, y, wmask, cmap=my_cmap)
    c = plt.contour(x, y, wcomposite[:], colors='k', levels=barlevels)
    plt.clabel(c, fontsize=7, inline=1, inline_spacing=5, fmt=frmt)
    plt.xticks(lon[3::40], lons[3::40])
    plt.gca().invert_yaxis()
    plt.savefig(fig_name, bbox_inches='tight')


def composite_hovmoller(data, x, y, significance_level, subplot=False, **kwargs):

    my_title = kwargs.pop('fig_title', '')
    fig_name = kwargs.pop('fig_name', 'my_picture.png')
    var_name = kwargs.pop('var_name', '')
    cmap = kwargs.pop('cmap', ('#000044', '#0033FF', '#007FFF', '#0099FF', '#00B2FF', '#00CCFF', '#FFFFFF',
                               '#FFFFFF', '#FFCC00', '#FF9900', '#FF7F00', '#FF3300', '#A50000', '#B48C82'))
    levels = kwargs.pop('levels', np.linspace(np.min(data), np.max(data), 13))
    # mask_data = kwargs.pop('mask_data', False)

    composite = np.nanmean(data, 0)
    sig = composites_t_test(data, significance_level)
    # if mask_data:
    #     Ptsgrid, lonlatgrid, Ptmask = Dg.pointinside(x, y, shapefile=mask_data, delimiter=' ')
    #     composite = np.ma.array(composite, mask=Ptmask)
    #     #sig = np.ma.array(sig, mask=Ptmask)

    if not subplot:
        fig = plt.figure(figsize=(12, 10))
        ax = fig.add_subplot(111)
    
    my_cmap = clr.ListedColormap(cmap)
    bar1 = cmap
    cmapover = cmap[-1]
    cmapunder = cmap[0]
    bar1 = bar1[1:-1]
    my_cmap = clr.ListedColormap(bar1)
    my_cmap.set_over(cmapover)
    my_cmap.set_under(cmapunder)
    norm = clr.BoundaryNorm(levels, my_cmap.N, clip=False)

    plt.title(my_title)
    c = plt.contourf(x, y, composite, cmap=my_cmap, levels=levels, norm=norm, extend='both')
    plt.contour(x, y, sig, linewidths=2, colors='k', levels=[0, 1])
    if not subplot:
        plt.colorbar(c, ticks=levels, orientation='horizontal', pad=0.05)
        plt.savefig(fig_name, bbox_inches='tight')
    else:
        return c


def hovmoller_difference(data1, data2, x, y, significance_level, subplot=False, **kwargs):

    my_title = kwargs.pop('fig_title', '')
    fig_name = kwargs.pop('fig_name', 'my_picture.png')
    var_name = kwargs.pop('var_name', '')
    cmap = kwargs.pop('cmap', ('#000044', '#0033FF', '#007FFF', '#0099FF', '#00B2FF', '#00CCFF', '#FFFFFF',
                               '#FFFFFF', '#FFCC00', '#FF9900', '#FF7F00', '#FF3300', '#A50000', '#B48C82'))
    levels = kwargs.pop('levels', np.linspace(np.min(np.nanmean(data1, 0) - np.nanmean(data2, 0)), np.max(np.nanmean(data1, 0) - np.nanmean(data2, 0)), 13))
    # mask_data = kwargs.pop('mask_data', False)

    composite = np.nanmean(data1, 0) - np.nanmean(data2, 0)
    sig = composites_t_diff(data1, data2, significance_level)
    # if mask_data:
    #     Ptsgrid, lonlatgrid, Ptmask = Dg.pointinside(x, y, shapefile=mask_data, delimiter=' ')
    #     composite = np.ma.array(composite, mask=Ptmask)
    #     #sig = np.ma.array(sig, mask=Ptmask)

    if not subplot:
        fig = plt.figure(figsize=(12, 10))
        ax = fig.add_subplot(111)
    
    my_cmap = clr.ListedColormap(cmap)
    bar1 = cmap
    cmapover = cmap[-1]
    cmapunder = cmap[0]
    bar1 = bar1[1:-1]
    my_cmap = clr.ListedColormap(bar1)
    my_cmap.set_over(cmapover)
    my_cmap.set_under(cmapunder)
    norm = clr.BoundaryNorm(levels, my_cmap.N, clip=False)

    plt.title(my_title)
    c = plt.contourf(x, y, composite, cmap=my_cmap, levels=levels, norm=norm, extend='both')
    plt.contour(x, y, sig, linewidths=2, colors='k', levels=[0, 1])
    if not subplot:
        plt.colorbar(c, ticks=levels, orientation='horizontal', pad=0.05)
        plt.savefig(fig_name, bbox_inches='tight')
    else:
        return c


def hovmoller(array, lat_array, lon_array, latmin, latmax, lonmin,
              lonmax, mean, lon_shift):

    """
    'Calcula a media meridional ou zonal'
    :param array:
    :param lat_array:
    :param lon_array:
    :param latmin:
    :param latmax:
    :param lonmin:
    :param lonmax:
    :param mean:
    :return:
    """

    ilatmin, ilonmin = gridindex(lat_array, lon_array, latmin, lonmin, lon_shift)
    ilatmax, ilonmax = gridindex(lat_array, lon_array, latmax, lonmax, lon_shift)

    lat = lat_array[ilatmin:ilatmax + 1]
    lon = lon_array[ilonmin:ilonmax + 1]

    if mean == 'lat':
        hov = np.nanmean(array[:, ilatmin:ilatmax + 1, ilonmin:ilonmax + 1],
                         axis=1)
    elif mean == 'lon':
        hov = np.nanmean(array[:, ilatmin:ilatmax + 1, ilonmin:ilonmax + 1],
                         axis=2)
    else:
        'Mean must be at lat or lon'
        hov = 0

    return hov, lat, lon


def plot_hovmoller(x, y, hov, levels='default', colors='default', barlims='neither'):

    if levels == 'default':
        levels = np.linspace(np.min(hov), np.max(hov), 13)
        # levels = [-2, -1.6, -1.2, -0.8, -0.4, -0.2, 0, 0.2, 0.4, 0.8, 1.2,
        #               1.6, 2]
    else:
        levels = levels

    if colors == 'default':
        colors = ('#0033FF', '#007FFF', '#0099FF', '#00B2FF', '#00CCFF',
                  '#C8FFFF', '#FFFFAA', '#FFCC00', '#FF9900', '#FF7F00',
                  '#FF3300', '#A50000',)
        cmap = clr.ListedColormap(colors)
        cmap.set_over('#B48C82')
        cmap.set_under('#000044')
    if barlims == 'max':
        colors = colors
        cmap = clr.ListedColormap(colors[0:-1])
        cmap.set_over(colors[-1])
    elif barlims == 'min':
        colors = colors
        cmap = clr.ListedColormap(colors[1:])
        cmap.set_under(colors[0])
    elif barlims == 'both':
        colors = colors
        cmap = clr.ListedColormap(colors[1:-1])
        cmap.set_under(colors[0])
        cmap.set_over(colors[-1])
    else:
        colors = colors
        cmap = clr.ListedColormap(colors[:])

    h = plt.contourf(x, y, hov, levels=levels, cmap=cmap, extend=barlims)
    # cbar = plt.colorbar(h, ticks=levels)
    return h
