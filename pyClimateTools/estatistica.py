# -*- coding: utf-8 -*-

import math
import numpy as np
from scipy import signal
from scipy.stats import t
from scipy.stats import chi2
from pyClimateTools.funcoes import rolling_window
from matplotlib import pyplot as plt
from numpy import linalg as LA


def regime_shift(data, l, p):

    """
    Aplica a técnica de determinação de mudança de regimes em séries temporais climáticas
    Artigo base:
    Rodinov, S. N., 2004: A sequential algorithm for testing climate regime shifts. Geophysical Research Letters, 31.

    Args:
    data: 1d array
        Série de dados que vai ser analisada

    l: int
        Intervalo de tempo mínimo em que se busca se há mudança de regime.

    p: float
        Nível de probabilidade para cálculo de significância. Ex: 10. == 10%

    """

    n = data.shape[0]
    df = 2*l - 2.
    t_crit = np.round(t.ppf(1 - (p/2.)/100., df), 2)
    var_mn = np.round(np.mean([np.var(data[i:i+l]) for i in np.arange(0, n-l+1)]), 2)
    std_mn = np.round(np.sqrt(var_mn), 2)
    diff = np.round(t_crit*np.sqrt(2*var_mn/l), 2)
    x_mn = np.round(np.mean(data[0:l]), 2)
    ll = l
    new_regime_fy = 0
    new_regime_ly = 10
    rsi = 0
    shift_index = []
    rsis = []

    while ll < n:
        if data[ll] > x_mn + diff:
            flag = 'p'
        elif data[ll] < x_mn - diff:
            flag = 'n'
        else:
            flag = '0'
        for i in np.arange(ll, ll+l):
            if i == n:
                break
            if flag == 'p':
                rsi += ((data[i] - (x_mn + diff)) / std_mn / l)
            elif flag == 'n':
                rsi += ((x_mn - diff - data[i]) / std_mn / l)
            else:
                if i > new_regime_ly:
                    x_mn = np.round(np.mean(data[new_regime_fy:i+1]), 2)
                break

            if i == ll+l-1:
                x_mn = np.round(np.mean(data[ll:ll+l]), 2)
                shift_index.append(ll)
                rsis.append(rsi)
                new_regime_fy = ll
                new_regime_ly = ll+l
                rsi = 0
                break
            if rsi < 0:
                rsi = 0
                if i > new_regime_ly:
                    x_mn = np.round(np.mean(data[new_regime_fy:i + 1]), 2)
                break
        ll += 1

    return shift_index, rsis


def filtro_121(data, n):

    """
    Filtra o dado de entrada usando através de média movel de três valores com pesos respectivos
    0.25, 0.5 e 0.25.

    Artigo base:


    Args:
    data: 1d array
        Série de dados que vai ser filtrado

    n: int
        Numero de vezes em que o filtro será passado. Quanto maior, mais a série fica alisada

    """

    filtered = np.full((n+1, len(data)), np.nan)
    filtered[0, :] = data

    for i in np.arange(n):
        for j in np.arange(0, len(data)):
            if j == 0:
                y = 0.5*filtered[i, j] + 0.5*filtered[i, j+1]
            elif j == len(data)-1:
                y = 0.5*filtered[i, j-1] + 0.5*filtered[i, j]
            else:
                y = 0.25*filtered[i, j-1] + 0.5*filtered[i, j] + 0.25*filtered[i, j+1]
            filtered[i+1, j] = y

    return filtered


def filtro_lanczos(data, window, cutoff1, cutoff2=0.):

    """
    Filtra o dado de entrada na frequencia cutoff1 (baixa frequencia), para
    um dado numero de pesos, e no inverso dessa frequencia (alta frequencia).
    Caso o cutoff2 seja passado, filtra o dado na banda entre cutoff1 e cutoff2.

    Link para os pesos de lanczos:
    https://scitools.org.uk/iris/docs/v1.2/examples/graphics/SOI_filtering.html

    Artigo base:
    Duchon, C. E., 1979: Lanczos filter in one and two dimensions. J. Applied Meteorology, 18, 1016–1022.

    Args:
    data: 1d array
        Série de dados que vai ser filtrado

    window: float
        Numero de pesos

    cutoff1: float
        A frequencia de corte 1 em inverso do passo no tempo

    cutoff2: float
        A frequencia de corte 2 em inverso do passo no tempo

    """

    order = ((window - 1) // 2) + 1
    nwts = 2 * order + 1
    w = np.zeros(int(nwts))
    n = nwts // 2
    w[int(n)] = 2 * (cutoff1 - cutoff2)
    k = np.arange(1., n)
    sigma = np.sin(np.pi * k / n) * n / (np.pi * k)
    firstfactor = (np.sin(2. * np.pi * cutoff1 * k) - np.sin(2. * np.pi * cutoff2 * k)) / (np.pi * k)
    w[int(n - 1):0:-1] = firstfactor * sigma
    w[int(n + 1):-1] = firstfactor * sigma

    yt_lowpass = np.full(data.shape, np.nan)
    yt_highpass = np.full(data.shape, np.nan)
    yt_bandpass = np.full(data.shape, np.nan)

    if cutoff2 == 0.:
    	yt_lowpass[window//2:-(window//2)] = np.sum(rolling_window(data, int(window)) * w[1:-1], 1)
    	yt_highpass = data - yt_lowpass

    else:
    	yt_bandpass[window//2:-(window//2)] = np.sum(rolling_window(data, int(window)) * w[1:-1], 1)

    return yt_lowpass, yt_highpass, yt_bandpass


def transf_fourier(dados, n_harmonicos):

    """
    Transforma uma série no domínio do tempo em uma série no domínio da frequencia pela transformada de Fourier

    Artigo base:
    Wilks, 1995, Chapter 8; Wilks 2011, chapter 9

    Args:
    data: 1d array
        Série de dados que vai ser transformada

    n_harmonicos: int
        Número de harmonicos que serão calculados

    """

    n_dados = len(dados)
    media = np.mean(dados)
    four_out = np.zeros((n_harmonicos, n_dados))
    cks = np.zeros((n_harmonicos))
    for l in np.arange(1, n_harmonicos+1):
        omega_k = (2 * math.pi * l) / n_dados
        yt1_cos = [np.cos(omega_k * (i + 1)) for i in np.arange(n_dados)]
        yt1_sin = [np.sin(omega_k * (i + 1)) for i in np.arange(n_dados)]
        a_k = (2. / n_dados) * np.sum([i * j for i, j in zip(dados, yt1_cos)])
        b_k = (2. / n_dados) * np.sum([i * j for i, j in zip(dados, yt1_sin)])
        c_k = np.sqrt(a_k**2 + b_k**2)
        cks[l-1] = c_k
        if a_k > 0:
            phi_ = np.arctan((b_k / a_k))
        elif a_k < 0:
            if np.arctan((b_k / a_k)) > 2*math.pi:
                phi_ = np.arctan((b_k / a_k)) - math.pi
            else:
                phi_ = np.arctan((b_k / a_k)) + math.pi
        else:
            phi_ = math.pi / 2.
        yt_cos = [(c_k * (np.cos(((omega_k * (i + 1)) - phi_)))) for i in np.arange(n_dados)]
        if l == 1:
            four_out[l-1, :] = yt_cos
        else:
            four_out[l-1, :] = four_out[l-2, :] + yt_cos

    four_out += media

    return four_out, cks


def detrend(data):

    detrend_data = signal.detrend(data[...], axis=0)

    return detrend_data


def t_test(sample, significance_level):

    n = sample.shape[0]
    df = n - 1
    t_crit = t.ppf(1 - (float(significance_level) / 2.) / 100., df)

    return t_crit


def composites_t_test(sample, significance_level):

    n = sample.shape[0]
    df = n - 1
    t_crit = t.ppf(1 - (float(significance_level) / 2.) / 100., df)
    sample_mean = np.nanmean(sample, axis=0)
    sample_std = np.nanstd(sample, axis=0)
    crit = (abs(sample_mean * np.sqrt(n) / sample_std) > t_crit)
    significance_mask = np.where(crit == True, 1, 0)

    return significance_mask


def composites_t_diff(sample1, sample2, significance_level):

    n1 = sample1.shape[0]
    n2 = sample1.shape[0]
    
    s1_mean = np.nanmean(sample1, axis=0)
    s1_std = np.nanstd(sample1, axis=0)
    s2_mean = np.nanmean(sample2, axis=0)
    s2_std = np.nanstd(sample2, axis=0)
    SD = s1_mean - s2_mean  # diferença das amostras (sample difference)
    SE = np.sqrt((s1_std**2/n1)+(s2_std**2/n2))  # Erro padrão (standard error)
    df = ((s1_std**2/n1)+(s2_std**2/n2))**2 / (((s1_std**2/n1)**2/(n1-1))+((s2_std**2/n2)**2/(n2-1)))
    t_crit = t.ppf(1 - (float(significance_level) / 2.) / 100., df)

    crit = abs(SD/SE) > t_crit
    significance_mask = np.where(crit == True, 1, 0)

    return significance_mask


def comp_yule_kendell_index(q25, q50, q75, iqr):

    yki = (q25 - 2*q50 + q75)/iqr

    return yki


def comp_skewness_coef(data, mn, std):

    n = len(data)
    sum_ = (np.sum([(i - mn)**3 for i in data]))/n
    cs = sum_/(std**3)

    return cs


def rmse(predictions, targets):
    #  Got from https://stackoverflow.com/questions/17197492/is-there-a-library-function-for-root-mean-square-error-rmse-in-python

    differences = predictions - targets                            #the DIFFERENCEs.

    differences_squared = differences ** 2                         #the SQUAREs of ^

    mean_of_differences_squared = np.nanmean(differences_squared)  #the MEAN of ^

    rmse_val = np.sqrt(mean_of_differences_squared)                #ROOT of ^

    return rmse_val                                                #get the ^


def analise_espectral(data, taper_percent, mean_window, p, plot=False, detrended=False):


    def split_cos_bell(data, percentage):

        n = data.shape[0]
        m = int(((percentage/100.) * n) / 2)

        data_taped = np.zeros(n)

        data_taped[0:m] = data[0:m] * [0.5*(1-np.cos(np.pi*(i-0.5)/m)) for i in np.arange(0, m)]
        data_taped[m:n-m] = data[m:n-m] * 1
        data_taped[n-m:n] = data[n-m:n] * [0.5*(1-np.cos(np.pi*(i-0.5)/m)) for i in np.arange(n-m, n)]

        return data_taped
    
    if detrended:
    	y2 = detrend(data)  # remove a tendência e a média da série
    else:
    	y2 = data
    y2 = split_cos_bell(y2, taper_percent)  # aplica o taper na serie)
    n = len(y2)
    n_ = int(n/2)
    tf = np.fft.fft(y2) / n
    raw_espec = np.abs(tf[0:n_]) ** 2
    if n % 2 == 0:
        k = np.arange(1, (n / 2) + 1)
    else:
        k = np.arange(1, ((n-1)/2)+1)
    freq = k/float(n)
    period = 1./freq

    espec = rolling_window(raw_espec, mean_window)  # aplica a media movel no dado, o peso deve ser impar
    espec = np.nanmean(espec, 1)

    neff = (1 - 1.27*taper_percent/100.)
    dof = 2*mean_window*neff
    acl1 = np.corrcoef(y2[:-1], y2[1:])[0, 1]  # auto-correlação de lag 1
    if acl1 == 0:
        theta = np.array([np.nanmean(espec) for i in k])
    else:
        theta = np.array([np.nanmean(espec)*( (1-(acl1**2)) / (1+(acl1**2)-(2*acl1*np.cos((np.pi*i)/(n/2)))) ) for i in k])

    chi5 = chi2.ppf(p/200., dof) / dof
    chi95 = chi2.ppf(1-(p/200.), dof) / dof
    l5 = theta*chi5
    l95 = theta*chi95

    # Plotagem
    if plot:
        fig, ax1 = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.semilogy(freq, espec)
        ax1.semilogy(freq, l5, label=u'Limite de confiança 5%')
        ax1.semilogy(freq, l95, label=u'Limite de confiança 95%')
        [ax1.text(freq[n]*2, 1.07, np.round(freq[n], 2), fontsize=8, rotation=70, 
         verticalalignment='top', transform=ax1.transAxes) for n in np.arange(len(freq)) if espec[n] > l95[n]]
        ax1.set_xlabel(u'Frequência')
        ax1.set_ylabel(u'Amplitudes Espectrais')
        ax1.set_xlim(0, np.max(freq))
        plt.legend()
        plt.show()
    else:
        return freq, espec, l5, l95


def eof(data, corr=True):

    if corr:
        r = data.corr(method='pearson')
    else:
        r = data.cov()
    
    l, e = LA.eig(r)  # l are the eigenvalues and e the eigenvectors
    
    PCs = []  # expansion coeficients of the EOFs
    for i in e:
        PCs.append(np.dot(data.values, i))

    return PCs, e, l
