# -*- coding: utf-8 -*-

import numpy as np
import netCDF4
import datetime
import matplotlib.pyplot as plt
from matplotlib.patches import Path, PathPatch
import matplotlib.patches as patches
import matplotlib.gridspec as gridspec
from mpl_toolkits.basemap import Basemap, shiftgrid
from scipy import stats
from dateutil.relativedelta import relativedelta
from mpl_toolkits.basemap import interp


__author__ = 'Enzo Pinheiro'
__email__ = 'pinheiroenzo92@gmail.com'
__license__ = 'GPL'
__version__ = '0.1'
__date__ = '11/07/2018'


def create_nc1d(ncname, var_array, time_array, var_units,
                time_units, var_standard_name, var_long_name,
                missing_value=-999., time_calendar='standard'):

    foo = netCDF4.Dataset(ncname, 'w')

    foo.createDimension('time', time_array.shape[0])

    times = foo.createVariable('time', 'f8', 'time')
    times.units = time_units
    times.calendar = time_calendar
    times[:] = time_array

    var = foo.createVariable(var_standard_name, 'f', ('time'))
    var.units = var_units
    var.long_name = var_long_name
    var.missing_value = missing_value
    var[:] = var_array

    foo.close()

    return


def create_nc3d(ncname, var_array, time_array, lat_array, lon_array, var_units,
                time_units, var_standard_name, var_long_name,
                missing_value=-999., time_calendar='standard',
                lat_units='degrees_north', lon_units='degrees_east',
                lat_long_name='latitude', lon_long_name='longitude'):

    foo = netCDF4.Dataset(ncname, 'w')

    foo.createDimension('time', time_array.shape[0])
    foo.createDimension('lat', lat_array.shape[0])
    foo.createDimension('lon', lon_array.shape[0])

    times = foo.createVariable('time', 'f8', 'time')
    times.units = time_units
    times.calendar = time_calendar
    times[:] = time_array

    laty = foo.createVariable('lat', 'f4', 'lat')
    laty.units = lat_units
    laty.long_name = lat_long_name
    laty[:] = lat_array

    lonx = foo.createVariable('lon', 'f4', 'lon')
    lonx.units = lon_units
    lonx.long_name = lon_long_name
    lonx[:] = lon_array

    var = foo.createVariable(var_standard_name, 'f', ('time', 'lat', 'lon'))
    var.units = var_units
    var.long_name = var_long_name
    var.missing_value = missing_value
    var[:] = var_array

    foo.close()

    return


def create_nc4d(ncname, var_array, time_array, lat_array, lon_array, z_array, var_units,
                time_units, var_standard_name, var_long_name,
                missing_value=-999., time_calendar='standard',
                lat_units='degrees_north', lon_units='degrees_east', z_units='mb',
                lat_long_name='latitude', lon_long_name='longitude', z_long_name='pressure level'):

    foo = netCDF4.Dataset(ncname, 'w')

    foo.createDimension('time', time_array.shape[0])
    foo.createDimension('z', z_array.shape[0])
    foo.createDimension('lat', lat_array.shape[0])
    foo.createDimension('lon', lon_array.shape[0])

    times = foo.createVariable('time', 'f8', 'time')
    times.units = time_units
    times.calendar = time_calendar
    times[:] = time_array

    zz = foo.createVariable('z', 'f4', 'z')
    zz.units = z_units
    zz.long_name = z_long_name
    zz[:] = z_array

    laty = foo.createVariable('lat', 'f4', 'lat')
    laty.units = lat_units
    laty.long_name = lat_long_name
    laty[:] = lat_array

    lonx = foo.createVariable('lon', 'f4', 'lon')
    lonx.units = lon_units
    lonx.long_name = lon_long_name
    lonx[:] = lon_array

    var = foo.createVariable(var_standard_name, 'f', ('time', 'z', 'lat', 'lon'))
    var.units = var_units
    var.long_name = var_long_name
    var.missing_value = missing_value
    var[:] = var_array

    foo.close()

    return


def mnclim(variable, time_variable, lat_variable, lon_variable, iyear, fyear,
           mask=False, std=False):
    time = time_variable
    lat = lat_variable
    lon = lon_variable
    itime = datetime.datetime.strptime(('01-01-' + str(iyear)), '%d-%m-%Y')
    iindex = netCDF4.date2index(itime, time)
    ftime = datetime.datetime.strptime(('01-12-' + str(fyear)), '%d-%m-%Y')
    findex = netCDF4.date2index(ftime, time)
    data = variable
    if mask == True:
        masked = np.ma.getmask(data[:])
        data = np.where(masked, np.NaN, data)

    data_clim = np.zeros((12, lat.shape[0], lon.shape[0]))

    if std:
        data_std = np.zeros((12, lat.shape[0], lon.shape[0]))
        for i in range(0, 12):
            data_clim[i, :, :] = np.nanmean(data[iindex + i:findex + 1:12, ...], 0)
            data_std[i, :, :] = np.nanstd(data[iindex + i:findex + 1:12, ...], 0)

        return data_clim, data_std

    else:    
        for i in range(0, 12):
            data_clim[i, :, :] = np.nanmean(data[iindex + i:findex + 1:12, ...], 0)

        return data_clim 


def mnanom(data_clim, variable, time_variable, lat_variable, lon_variable, imon,
           iyear, fmon, fyear, mask=False, norm=[]):
    clim_data = data_clim

    lat = lat_variable
    lon = lon_variable
    time = time_variable
    index2time = netCDF4.num2date(time[:], time.units)
    itime = datetime.datetime.strptime(('01-{0}-{1}'.format(imon, iyear)),
                                       '%d-%m-%Y')
    iindex = netCDF4.date2index(itime, time)
    ftime = datetime.datetime.strptime(('01-{0}-{1}'.format(fmon, fyear)),
                                       '%d-%m-%Y')
    findex = netCDF4.date2index(ftime, time)
    data = variable
    if mask == True:
        masked = np.ma.getmask(data[:])
        data = np.where(masked, np.NaN, data)

    # calculo anomalia
    data_anom = np.zeros(data[iindex:findex + 1, :, :].shape)
    if norm != []:
        k = 0
        for r in range(iindex, findex + 1):
            mon = index2time[r].month - 1
            data_anom[k, :, :] = (data[r, ...] - clim_data[mon, ...])/norm[mon, ...]
            k += 1
    else:
        k = 0
        for r in range(iindex, findex + 1):
            mon = index2time[r].month - 1
            data_anom[k, :, :] = data[r, ...] - clim_data[mon, ...]
            k += 1

    ntime = time[iindex:findex + 1]

    return data_anom, ntime


def region_index(ilat, ilon, flat, flon, lat_array, lon_array, data_array, lon_shift=0.):

    """
    Calcula a média da variável de entrada para um região específica

    Args:
    ilat: float
        latitude incial da região
    ilon: float
        longitude inicial do região
    flat: float
        latitude final do região
    flon: float
        longitude final do região
    lat_array: 1d array
        vetor contendo as latitudes
    lon_array: 1d array
        vetor contendo as longitudes
    data_array: nd array
        matriz do dado em grade

    """

    ilat_min, ilon_min = gridindex(lat_array, lon_array, ilat, ilon, lon_shift)
    ilat_max, ilon_max = gridindex(lat_array, lon_array, flat, flon, lon_shift)

    index = np.nanmean(data_array[..., ilat_min:ilat_max+1, ilon_min:ilon_max+1], axis=np.s_[1, 2])

    return index


def comp_5yr_window_anom(data):

    nyrs = data[::12, ...].shape[0]
    nclims = (nyrs/5) - 5
    nlat = data.shape[1]
    nlon = data.shape[2]
    clims = np.zeros((nclims, 12, data.shape[1], data.shape[2]))
    anom = np.zeros(data.shape)

    for j in np.arange(0, nclims):
        init_j = j * 5 * 12
        final_j = init_j + 360
        for i in np.arange(0, 12):
            monclim = np.ma.mean(data[init_j+i:final_j+i:12, ...], 0)
            clims[j, i, ...] = monclim[:]

    if nclims == 1:
        anom[:] = data[:] - np.repeat(clims[None, 0, ...], nyrs, 0).reshape((nyrs*12, nlat, nlon))
    else:
        # Anomalias de 1900-1920 utilizando a climatologia de 1900-1930
        anom[0:252] = data[0:252] - np.repeat(clims[None, 0, ...], 21, 0).reshape((21*12, nlat, nlon))
        # Anomalias de 1921 - 1995
        kk = 252
        for k in np.arange(1, nclims-1):
            anom[kk:kk+60] = data[kk:kk+60] - np.repeat(clims[None, k, ...], 5, 0).reshape((5*12, nlat, nlon))
            kk += 60
        # Anomalias de 1996 - 2010
        ll = data.shape[0] - kk
        anom[kk:] = data[kk:] - np.repeat(clims[None, nclims-1, ...], ll/12., 0).reshape((ll, nlat, nlon))

    return clims, anom


def comp_5yr_window_tseries_anom(data):

    nyrs = data[::12, ...].shape[0]
    nclims = (nyrs/5) - 5
    clims = np.zeros((nclims, 12))
    anom = np.zeros(data.shape)

    for j in np.arange(0, nclims):
        init_j = j * 5 * 12
        final_j = init_j + 360
        for i in np.arange(0, 12):
            monclim = np.ma.mean(data[init_j+i:final_j+i:12, ...], 0)
            clims[j, i] = monclim

    if nclims == 1:
        anom[:] = data[:] - np.repeat(clims[None, 0, ...], nyrs, 0).reshape((nyrs*12))
    else:
        # Anomalias de 1911-1920 utilizando a climatologia de 1900-1930
        anom[0:120] = data[0:120] - np.repeat(clims[None, 0, ...], 10, 0).reshape((10*12))
        # Anomalias de 1921 - 1995
        kk = 120
        for k in np.arange(1, nclims-1):
            anom[kk:kk+60] = data[kk:kk+60] - np.repeat(clims[None, k, ...], 5, 0).reshape((5*12))
            kk += 60
        # Anomalias de 1996 - 2010
        ll = data.shape[0] - kk
        anom[kk:] = data[kk:] - np.repeat(clims[None, nclims-1, ...], ll/12., 0).reshape((ll))

    return clims, anom


def rolling_window(data, window):

    """
    Cria várias linhas com n valores, onde n é a janela utilizada, centrado no meio da janela

    :param data:
    :param window:
    :return:
    """

    if window % 2 == 0:
        n_2 = int((window)/2)  # 1
        data_ = np.array([data[i-n_2:i+n_2] for i in np.arange(n_2, data.shape[0] - n_2 + 1)])
    else:
        n_2 = int((window-1)/2)  # 1
        data_ = np.array([data[i-n_2:i+n_2+1] for i in np.arange(n_2, data.shape[0] - n_2)])

    return data_


def read_ncs(input_nc, shift=False):

    inc = netCDF4.Dataset(input_nc)

    if len(inc.variables.keys()) == 5:
        lat = inc.variables['lat'][::-1]
        lon = inc.variables['lon'][:]
        time = inc.variables['time']
        dates = netCDF4.num2date(time[:], time.units)
        levels = inc.variables['z'][:]
        var = inc.variables[inc.variables.keys()[-1]][:, :, ::-1, :]
        if shift:
            var, lon = shiftgrid(shift, var, lon, False)
        inc.close()

        return lat, lon, dates, levels, var

    else:
        lat = inc.variables['lat'][::-1]
        lon = inc.variables['lon'][:]
        time = inc.variables['time']
        dates = netCDF4.num2date(time[:], time.units)
        var = inc.variables[inc.variables.keys()[-1]][:, ::-1, :]
        if shift:
            var, lon = shiftgrid(shift, var, lon, False)
        inc.close()

        return lat, lon, dates, var


# def pol(ilon, flon, ilat, flat, fig_instance, (coluna, linha, posicao), color='none', hatch='none'):
#     """
#     Desenha um poligno no plot dada as coordenadas dos vertices.
#     :param ilon:
#     :param flon:
#     :param ilat:
#     :param flat:
#     :return:
#     """

#     verts = [
#         (ilon, ilat),  # esquerda, base
#         (ilon, flat),  # esquerda, topo
#         (flon, flat),  # direita, topo
#         (flon, ilat),  # direita, base
#         (ilon, ilat),  # fechar poligono
#     ]

#     codes = [
#         Path.MOVETO,
#         Path.LINETO,
#         Path.LINETO,
#         Path.LINETO,
#         Path.CLOSEPOLY,
#     ]
#     path = Path(verts, codes)
#     ax = fig_instance.add_subplot(coluna, linha, posicao)
#     patch = patches.PathPatch(path, edgecolor=color, hatch=hatch, facecolor='none', lw=1.5)
#     ax.add_patch(patch)


def gridindex(lat_array, lon_array, ilat, ilon, lon_shift=0.):

    if lon_shift == 0:
        # 1) somar o shift e diminuir 180. da matriz de longitude
        lon_array = lon_array - 180 + lon_shift
        
        # 2) encontrar o indice nessa nova matriz
        lat_idx = (np.abs(lat_array - ilat)).argmin()
        lon_idx = (np.abs(lon_array - ilon)).argmin()
        
        # 3) converter o valor do shift em numero de pontos e somar ao indice
        grid_resol = lon_array[1] - lon_array[0]
        n_points = (-180+lon_shift)/grid_resol
        lon_idx = lon_idx + n_points

        # Caso o lon_idx passe do valor máximo, volta para o começo
        if lon_idx >= len(lon_array):
            lon_idx = lon_idx - len(lon_array)

    else:
        # 1) diminuir o shift e somar 180. da matriz de longitude
        lon_array = lon_array + 180 - lon_shift
        
        # 2) encontrar o indice nessa nova matriz
        lat_idx = (np.abs(lat_array - ilat)).argmin()
        lon_idx = (np.abs(lon_array - ilon)).argmin()
        
        # 3) converter o valor do shift em numero de pontos e somar ao indice
        grid_resol = lon_array[1] - lon_array[0]
        n_points = (180-lon_shift)/grid_resol
        lon_idx = lon_idx + n_points

        # Caso o lon_idx passe do valor máximo, volta para o começo
        if lon_idx >= len(lon_array):
            lon_idx = lon_idx - len(lon_array)


    return int(lat_idx), int(lon_idx)

def date2index(dates, time_array, time_units):
    """
    Compute the index in time array for a given date.

    :param dates: input datetime
    :type dates: datetime
    :param time_array: input netCDF time variable
    :type time_array: netCDF variable
    :return: index for input date
    """

    if len(time_units.split(' ')) == 4:
        itime = datetime.datetime.strptime(time_units.split(' ')[2] +
                                           time_units.split(' ')[3],
                                           '%Y-%m-%d%H:%M:%S')
        if itime.hour == 12:
            itime = itime - relativedelta(hours=12)
    else:
        itime = datetime.datetime.strptime(time_units.split(' ')[2],
                                           '%Y-%m-%d')

    if time_units.split(' ')[0] == 'hours':
        delta_time = relativedelta(dates, itime).hours
        date_index = np.where(time_array[:] == delta_time)[0][0]
    elif time_units.split(' ')[0] == 'days':
        delta_time = (dates - itime).days
        date_index = np.where(time_array[:] == delta_time)[0][0]
    elif time_units.split(' ')[0] == 'months':
        delta_time = 12*(dates.year - itime.year) + \
                     (dates.month - itime.month)
        date_index = np.where(time_array[:] == delta_time)[0][0]
    elif time_units.split(' ')[0] == 'years':
        delta_time = relativedelta(dates, itime).years
        date_index = np.where(time_array[:] == delta_time)[0][0]
    else:
        date_index = 0
        exit()

    return date_index


def interpolate2grid(variable, lat_values, lon_values, new_lat, new_lon):
    # Must be 3d array (time, lat, lon)

    x, y = np.meshgrid(new_lon, new_lat)

    ntime = variable.shape[0]

    new_data = np.zeros((ntime, new_lat.shape[0], new_lon.shape[0]))

    for i in range(0, ntime):
        new_data[i, :, :] = interp(variable[i, :, :], lon_values, lat_values, x, y, order=1)

    return new_data


def colormap_viewer(colormaps):

    """
    """

    if colormaps == 'examples':
        atsm1 = ('#000044', '#0033FF', '#007FFF', '#0099FF', '#00B2FF', '#00CCFF',
                 '#FFFFFF', '#FFFFFF', '#FFCC00', '#FF9900', '#FF7F00', '#FF3300',
                 '#A50000', '#B48C82')

        cinza = ('#ffffff', '#E5E5E5', '#CBCBCB', '#B2B2B2', '#989898', '#7F7F7F',
                 '#666666', '#4E4E4E')

        wind = ('#FFFFFF', '#00CCFF', '#0099FF', '#0000FF', '#FFB200', '#FF0000')

        correl = ('#00FFFF', '#0DCAFF', '#1A94FF', '#285EFF', '#4141FF', '#8989FF',
                  '#D1D1FF', '#FFFFFF', '#FFFFFF', '#FFFFFF',
                  '#FFFFFF', '#FFFFFF', '#FFFFFF', '#FFD1D1', '#FF8989', '#FF4141',
                  '#FF5E28', '#FF9417', '#FFCA0D', '#FFFF00')

        obs = ('#4B004B', '#960096', '#B400B4', '#C800C8', '#6400FF', '#0032E1',
               '#0096E1', '#00FFFF', '#FFFFFF',
               '#32E100', '#96E100', '#E1E100', '#FFFF00', '#FFAA00', '#FF8700',
               '#FF6400', '#C80000')

        ylwred12 = ('#FFFFB4', '#FFFF8C', '#FFFF64', '#FFF000', '#FFC800',
                    '#FFA000', '#FF7800', '#FF5000', '#FF0000', '#B40000',
                    '#A00000', '#B48C82')

        paletas = [atsm1, cinza, wind, correl, obs]

        colormaps = paletas

    else:
        colormaps = [colormaps]

    fig = plt.figure(figsize=(6, len(colormaps)))

    for i, cmaps in enumerate(colormaps):
        ax = fig.add_subplot(len(colormaps), 1, i+1)
        try:
            cmap = colors.ListedColormap(cmaps)
        except:
            cmap = cmaps
        norm = colors.Normalize(vmin=0, vmax=cmap.N)
        colorbar.ColorbarBase(ax, cmap=cmap, orientation='horizontal', norm=norm,
                              ticks=np.arange(0, cmap.N + 1))

        ax.set_axis_off()
    fig.tight_layout()

    plt.show()


def layout_test(x, y, size, nsubplots, nrow, ncol, cbar_settings, layout_settings, skip_rows=[], skip_cols=[], texts=[], bm=False):

    """
    x (array):
    y (array):
    size (tuple): tamanho da figura
    nrow (int): numero de linhas
    ncol (int): numero de colunas
    cbar_setting (list): posicao da barra de cores, i.e.: [xpos, ypos, width, heigh]
    layout_settings (list): configuracoes do frame onde ficam as figuras, i.e.: [xi, yi, xf, yf]
    skip_rows (list): linhas que vao ser ignoradas
    skip_cols (list): colunas que vao ser ignoradas
    texts (list): lista com textos e suas posicoes, i.e.: [[pos_x1, pos_y1, text1], [pos_x2, pos_y2, text2]]
    bm (bool): Turn basemap on (True) or Off
    """

    x_, y_ = np.meshgrid(x, y)
    array = np.random.rand(len(y), len(x))
    
    fig, ax = plt.subplots(figsize=size)
    gs = gridspec.GridSpec(nrow, ncol)
    real_nrow = nrow - len(skip_rows)
    real_ncol = ncol - len(skip_cols)
    if nrow > ncol:
        posx = np.split(np.array([n for n in range(nrow) if n not in skip_rows]), nsubplots/real_ncol)
        posy = np.split(np.array([n for n in range(ncol) if n not in skip_cols]), real_ncol)
    elif nrow < ncol:
        posx = np.split(np.array([n for n in range(nrow) if n not in skip_rows]), real_nrow)
        posy = np.split(np.array([n for n in range(ncol) if n not in skip_cols]), nsubplots/real_nrow)

    for i in range(len(posx)):
        for j in range(len(posy)):
            plt.subplot(gs[posx[i][0]:posx[i][-1]+1, posy[j][0]:posy[j][-1]+1]) 
            if bm:
                m = Basemap(np.min(x), np.min(y), np.max(x), np.max(y))
                m.drawcoastlines()
                m.drawparallels(np.arange(-90, 100, 10), labels=[1, 0, 1, 0], linewidth=0.1)
                m.drawmeridians(np.arange(-180, 210, 30), labels=[0, 1, 0, 1], linewidth=0.1)
            c = plt.contourf(x_, y_, array, cmap=plt.get_cmap('RdBu'))

    if texts != []:
        [plt.text(i[0], i[1], i[2], transform=ax.transAxes, fontsize=16) for i in texts]
    cbar_ax_2 = fig.add_axes(cbar_settings)
    cb = plt.colorbar(c, orientation='vertical', cax=cbar_ax_2)
    plt.tight_layout(rect=layout_settings)
    plt.savefig('layout_test.tif')


def cut2shapefile(plot_obj, shape_obj, shift=0):

    """
    Shows data only inside a polygon, masking all the rest

    plot_obj: axis where plot is being made. ex: ax
    shape_obj: basemap shapefile. ex: m.nordeste_do_brasil when shape is read with m.readshapefile(path/to/nordeste_do_brasil, nordeste_do_brasil)
    """

    x0,x1 = plot_obj.get_xlim()
    y0,y1 = plot_obj.get_ylim()
    
    edges = [(x0, y0), (x1, y0), (x1, y1), (x0, y1), (x0, y0)]
    edge_codes = [Path.MOVETO] + (len(edges) - 1) * [Path.LINETO]
    
    verts = [(i[0]+shift, i[1]) for i in shape_obj[0]]
    verts = verts + [verts[0]]
    codes = [Path.MOVETO] + (len(verts) - 1) * [Path.LINETO]
    
    path = Path(verts+edges, codes+edge_codes)

    patch = PathPatch(path, facecolor='gray', lw=0)
    plot_obj.add_patch(patch)
