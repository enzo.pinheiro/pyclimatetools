# -*- coding: utf-8 -*-

import numpy as np
from pyClimateTools.funcoes import rolling_window


def select_nino(son_nino34, ond_nino34, ndj_nino34, djf_nino34, weak_crit, medium_crit, strong_crit):

    """ Select El Niño and La Ninã events when all 4 seasonal running mean from SON to DJF are greater than 0.5 °C
    and lower than -0.5 °C, respectively. It also classifies on weak, medium and strong events based on the 
    threshold given. 
    """

    elnino = [[], [], []]
    lanina = [[], [], []]
    neutro = []
    for son, ond, ndj, djf, t in zip(son_nino34, ond_nino34, ndj_nino34,
                                     djf_nino34, range(0, len(son_nino34))):
        season_mean = np.mean((son, ond, ndj, djf))
        # El Nino
        if son >= 0.5 and ond >= 0.5 and ndj >= 0.5 and djf >= 0.5:
            if weak_crit <= season_mean <= medium_crit:
                elnino[0].append(t)
            elif medium_crit <= season_mean <= strong_crit:
                elnino[1].append(t)
            elif season_mean >= strong_crit:
                elnino[2].append(t)
        elif son <= -0.5 and ond <= -0.5 and ndj <= -0.5 and djf <= -0.5:
            # La Nina
            if -weak_crit >= season_mean >= -medium_crit:
                lanina[0].append(t)
            elif -medium_crit >= season_mean >= -strong_crit:
                lanina[1].append(t)
            elif season_mean <= -strong_crit:
                lanina[2].append(t)
        else:
            neutro.append(t)
    return elnino, lanina, neutro


def oni_index(data_nino34, dates, norm=False):

    """
    Calcula o Oceanic Nino Index (ONI) baseado em uma série do Niño 3.4

    Artigo base:
    Rebecca, L., 2013: In Watching for El Niño and La Niña, NOAA adapts to global warming.
    Em: https://www.climate.gov/news-features/understanding-climate/watching-el-ni%C3%B1o-and-la-ni%C3%B1a-noaa-adapts-global-warming

    Args:
    data_nino34: 1d array
        Série de dados do Niño 3.4 de entrada que vai ser analisada

    """

    nyrs = len(data_nino34[::12])
    nclims = (nyrs/5) - 5
    yr = np.arange(1900, 1980, 5)

    clims = np.zeros((nclims, 12))
    stds = np.zeros((nclims, 12))
    anom = np.zeros(len(data_nino34))

    for jj, j in enumerate(np.arange(0, (nclims*5*12), 5*12)):
        for i in np.arange(0, 12):
            mon = np.nanmean(data_nino34[(j+i):(j+i)+(30*12)+1:12])
            std = np.nanstd(data_nino34[(j+i):(j+i)+(30*12)+1:12])
            clims[jj, i] = mon
            stds[jj, i] = std
    if norm:
        # Anomalias de 1900-1920 utilizando a climatologia de 1900-1930
        anom[0:252] = (data_nino34[0:252] - np.tile(clims[0, :], 21))/np.tile(stds[0, :], 21)
        # Anomalias de 1921 - 1995
        kk = 252
        for k in np.arange(1, nclims-1):
            anom[kk:kk+60] = (data_nino34[kk:kk+60] - np.tile(clims[k, :], 5))/np.tile(stds[k, :], 5)
            kk += 60
        # Anomalias de 1996 - 2010
        anom[kk:] = (data_nino34[kk:] - np.tile(clims[16, :], 15))/np.tile(stds[16, :], 15)
    else:
        # Anomalias de 1900-1920 utilizando a climatologia de 1900-1930
        anom[0:252] = data_nino34[0:252] - np.tile(clims[0, :], 21)
        # Anomalias de 1921 - 1995
        kk = 252
        for k in np.arange(1, nclims-1):
            anom[kk:kk+60] = data_nino34[kk:kk+60] - np.tile(clims[k, :], 5)
            kk += 60
        # Anomalias de 1996 - 2010
        anom[kk:] = data_nino34[kk:] - np.tile(clims[16, :], 15)

    return anom, clims


def nct_nwp_classification(nino3, nino4, dates):

    """
    Niño cold tongue (NCT) and Niño warm pool (NWP) indices are calculated following Ren and Jin (2011). 
    Events are selected when at least 4 seasonal running mean between SON and MAM are above 1 std of each index.
    When an year is selected by both indices, preference is given to the greater extended winter mean [OND, NDJ, DJF, JFM] index.
    """

    param = np.where(nino3*nino4 > 0., 2./5., 0.)

    nct = nino3 - param*nino4
    nwp = nino4 - param*nino3

    # Ninos classification
    nct = nct/np.std(nct)
    nwp = nwp/np.std(nwp)

    nct_trim = np.nanmean(rolling_window(nct, 3), 1)
    nwp_trim = np.nanmean(rolling_window(nwp, 3), 1)

    nct_roll = rolling_window(nct_trim, 4)
    nct_winter = np.array([np.array(nct_roll[i:i+4]) for i in np.arange(8, nct_roll.shape[0], 12)])  ## [[SON, OND, NDJ, DJF], [OND, NDJ, DJF, JFM], [NDJ, DJF, JFM, FMA], [DJF, JFM, FMA, MAM]]
    nct_djf_mean = [np.mean(i[1]) for i in nct_winter]

    nwp_roll = rolling_window(nwp_trim, 4)
    nwp_winter = np.array([np.array(nwp_roll[i:i+4]) for i in np.arange(8, nwp_roll.shape[0], 12)])  ## [[SON, OND, NDJ, DJF], [OND, NDJ, DJF, JFM], [NDJ, DJF, JFM, FMA], [DJF, JFM, FMA, MAM]]
    nwp_djf_mean = [np.mean(i[1]) for i in nwp_winter]

    nct_yrs = np.array([dates[::12][i].year for i in np.arange(nct_winter.shape[0])
                                            if np.array([(j >= 1.).all() for j in nct_winter[i]]).any()])
    nwp_yrs = np.array([dates[::12][i].year for i in np.arange(nwp_winter.shape[0])
                                            if np.array([(j >= 1.).all() for j in nwp_winter[i]]).any()])

    for n, i in enumerate(nct_yrs):
        if i in nwp_yrs:
            if nct_djf_mean[n] > nwp_djf_mean[n]:
                nwp_yrs = [j for j in nwp_yrs if j != i]
            else:
                nct_yrs = [j for j in nct_yrs if j != i]

    return np.array(nct_yrs), np.array(nwp_yrs), nct, nwp
