from setuptools import setup

setup(name='pyClimateTools',
      version='0.2',
      description='',
      url='',
      author='Enzo Pinheiro',
      author_email='pinheiroenzo92@gmail.com',
      license='MIT',
      packages=['pyClimateTools'],
      zip_safe=False)
